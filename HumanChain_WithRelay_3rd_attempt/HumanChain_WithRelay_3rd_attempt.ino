int ledPin = 13;      // select the pin for the LED
int firstPin = 2;      // select the pin for the LE
int relPins[5] = {2,3,4,5,6};      // select the pin for the 
int analogPins[5] = {A0,A1,A2,A3,A4};      // select the pin for the LED
int analogReads[5] = {0,0,0,0,0};      // select the pin for the LED

unsigned long timer = 0;
unsigned long timer_threshold = 1000;

int relay_progression_counter = 0;
int relay_progression_counter_next = 1;

short touch_flags[10] = {0,0,0,0,0,0,0,0,0,0};

int delay_time = 100;
int delay_time_first_flip = 50;

#define DEBUG

void setup() {
  // declare the ledPin as an OUTPUT:
  pinMode(ledPin, OUTPUT);
  for(int k = 0; k<5; k++)
  {
    pinMode(relPins[k], OUTPUT);
    digitalWrite(relPins[k], HIGH);
  }
  digitalWrite(firstPin, HIGH);
  Serial.begin(115200);
}

void loop() {

// ------------------------------------------  SECTION 0 ----------------------//
  digitalWriteFast(relPins[0], LOW);
  digitalWriteFast(relPins[1], HIGH);
  digitalWriteFast(relPins[2], HIGH);
  digitalWriteFast(relPins[3], HIGH);
  digitalWriteFast(relPins[4], HIGH);
  delay(delay_time_first_flip);

  analogReads[0] = analogRead(relPins[0]);
  if(analogReads[0] < 1010)
  {
    //Flip all pins
    for(int k = 0; k<5; k++)
    {
      digitalWriteFast(relPins[k], !digitalReadFast(relPins[k]) );
    }
    
    //Wait some time fo rthe relay to switch
    delay(delay_time);
    
    //Read All Other Pins
    analogReads[1] = analogRead(relPins[1]);
    analogReads[2] = analogRead(relPins[2]);
    analogReads[3] = analogRead(relPins[3]);
    analogReads[4] = analogRead(relPins[4]);

    if(analogReads[1] < 1010)
    {
      touch_flags[0] = 1;
    } else
      {
        touch_flags[0] = 0;
      }
    if(analogReads[2] < 1010)
    {
      touch_flags[1] = 1;
    } else
      {
        touch_flags[1] = 0;
      }
    if(analogReads[3] < 1010)
    {
      touch_flags[2] = 1;
    } else
      {
        touch_flags[2] = 0;
      }
    if(analogReads[4] < 1010)
    {
      touch_flags[3] = 1;
    } else
      {
        touch_flags[3] = 0;
      }

    #ifdef DEBUG
      for(int k = 0; k<5; k++)
      {
        Serial.print(analogReads[k]); 
        Serial.print(" ");  
      } 
      Serial.println();
    #endif
  }
// ------------------------------------------ END  SECTION 0 ----------------------//

// ------------------------------------------  SECTION 1 ----------------------//
  digitalWriteFast(relPins[0], HIGH);
  digitalWriteFast(relPins[1], LOW);
  digitalWriteFast(relPins[2], HIGH);
  digitalWriteFast(relPins[3], HIGH);
  digitalWriteFast(relPins[4], HIGH);
    delay(delay_time_first_flip);


  analogReads[1] = analogRead(relPins[1]);
  if(analogReads[1] < 1010)
  {
    //Flip all pins
    for(int k = 0; k<5; k++)
    {
      digitalWriteFast(relPins[k], !digitalReadFast(relPins[k]) );
    }
    
    //Wait some time fo rthe relay to switch
    delay(delay_time);
    
    //Read All Other Pins
    analogReads[0] = analogRead(relPins[0]);
    analogReads[2] = analogRead(relPins[2]);
    analogReads[3] = analogRead(relPins[3]);
    analogReads[4] = analogRead(relPins[4]);

    if(analogReads[0] < 1010)
    {
      touch_flags[0] = 1;
    } else
      {
        touch_flags[0] = 0;
      }
    if(analogReads[2] < 1010)
    {
      touch_flags[4] = 1;
    } else
      {
        touch_flags[4] = 0;
      }
    if(analogReads[3] < 1010)
    {
      touch_flags[5] = 1;
    } else
      {
        touch_flags[5] = 0;
      }
    if(analogReads[4] < 1010)
    {
      touch_flags[6] = 1;
    } else
      {
        touch_flags[6] = 0;
      }

    #ifdef DEBUG
      for(int k = 0; k<5; k++)
      {
        Serial.print(analogReads[k]); 
        Serial.print(" ");  
      } 
      Serial.println();
    #endif
  }
// ------------------------------------------ END  SECTION 1 ----------------------//

// ------------------------------------------  SECTION 2 ----------------------//
  digitalWriteFast(relPins[0], HIGH);
  digitalWriteFast(relPins[1], HIGH);
  digitalWriteFast(relPins[2], LOW);
  digitalWriteFast(relPins[3], HIGH);
  digitalWriteFast(relPins[4], HIGH);
    delay(delay_time_first_flip);


  analogReads[2] = analogRead(relPins[2]);
  if(analogReads[2] < 1010)
  {
    //Flip all pins
    for(int k = 0; k<5; k++)
    {
      digitalWriteFast(relPins[k], !digitalReadFast(relPins[k]) );
    }
    
    //Wait some time fo rthe relay to switch
    delay(delay_time);
    
    //Read All Other Pins
    analogReads[0] = analogRead(relPins[0]);
    analogReads[1] = analogRead(relPins[1]);
    analogReads[3] = analogRead(relPins[3]);
    analogReads[4] = analogRead(relPins[4]);

    if(analogReads[0] < 1010)
    {
      touch_flags[1] = 1;
    } else
      {
        touch_flags[1] = 0;
      }
    if(analogReads[1] < 1010)
    {
      touch_flags[4] = 1;
    } else
      {
        touch_flags[4] = 0;
      }
    if(analogReads[3] < 1010)
    {
      touch_flags[7] = 1;
    } else
      {
        touch_flags[7] = 0;
      }
    if(analogReads[4] < 1010)
    {
      touch_flags[8] = 1;
    } else
      {
        touch_flags[8] = 0;
      }

    #ifdef DEBUG
      for(int k = 0; k<5; k++)
      {
        Serial.print(analogReads[k]); 
        Serial.print(" ");  
      } 
      Serial.println();
    #endif
  }
// ------------------------------------------ END  SECTION 2 ----------------------//

// ------------------------------------------  SECTION 3 ----------------------//
  digitalWriteFast(relPins[0], HIGH);
  digitalWriteFast(relPins[1], HIGH);
  digitalWriteFast(relPins[2], HIGH);
  digitalWriteFast(relPins[3], LOW);
  digitalWriteFast(relPins[4], HIGH);
    delay(delay_time_first_flip);


  analogReads[3] = analogRead(relPins[3]);
  if(analogReads[3] < 1010)
  {
    //Flip all pins
    for(int k = 0; k<5; k++)
    {
      digitalWriteFast(relPins[k], !digitalReadFast(relPins[k]) );
    }
    
    //Wait some time fo rthe relay to switch
    delay(delay_time);
    
    //Read All Other Pins
    analogReads[0] = analogRead(relPins[0]);
    analogReads[1] = analogRead(relPins[1]);
    analogReads[2] = analogRead(relPins[2]);
    analogReads[4] = analogRead(relPins[4]);

    if(analogReads[0] < 1010)
    {
      touch_flags[2] = 1;
    } else
      {
        touch_flags[2] = 0;
      }
    if(analogReads[1] < 1010)
    {
      touch_flags[5] = 1;
    } else
      {
        touch_flags[5] = 0;
      }
    if(analogReads[2] < 1010)
    {
      touch_flags[7] = 1;
    } else
      {
        touch_flags[7] = 0;
      }
    if(analogReads[4] < 1010)
    {
      touch_flags[9] = 1;
    } else
      {
        touch_flags[9] = 0;
      }

    #ifdef DEBUG
      for(int k = 0; k<5; k++)
      {
        Serial.print(analogReads[k]); 
        Serial.print(" ");  
      } 
      Serial.println();
    #endif
  }
// ------------------------------------------ END  SECTION 3 ----------------------//

// ------------------------------------------  SECTION 4 ----------------------//
  digitalWriteFast(relPins[0], HIGH);
  digitalWriteFast(relPins[1], HIGH);
  digitalWriteFast(relPins[2], HIGH);
  digitalWriteFast(relPins[3], HIGH);
  digitalWriteFast(relPins[4], LOW);
    delay(delay_time_first_flip);


  analogReads[4] = analogRead(relPins[4]);
  if(analogReads[4] < 1010)
  {
    //Flip all pins
    for(int k = 0; k<5; k++)
    {
      digitalWriteFast(relPins[k], !digitalReadFast(relPins[k]) );
    }
    
    //Wait some time fo rthe relay to switch
    delay(delay_time);
    
    //Read All Other Pins
    analogReads[0] = analogRead(relPins[0]);
    analogReads[1] = analogRead(relPins[1]);
    analogReads[2] = analogRead(relPins[2]);
    analogReads[3] = analogRead(relPins[3]);

    if(analogReads[0] < 1010)
    {
      touch_flags[3] = 1;
    } else
      {
        touch_flags[3] = 0;
      }
    if(analogReads[1] < 1010)
    {
      touch_flags[6] = 1;
    } else
      {
        touch_flags[6] = 0;
      }
    if(analogReads[2] < 1010)
    {
      touch_flags[8] = 1;
    } else
      {
        touch_flags[8] = 0;
      }
    if(analogReads[3] < 1010)
    {
      touch_flags[9] = 1;
    } else
      {
        touch_flags[9] = 0;
      }

    #ifdef DEBUG
      for(int k = 0; k<5; k++)
      {
        Serial.print(analogReads[k]); 
        Serial.print(" ");  
      } 
      Serial.println();
    #endif
  }
// ------------------------------------------ END  SECTION 4 ----------------------//
  
  int sum = 0;
  for(int i = 0; i<10; i++)
  {
    sum += touch_flags[i]*pow(2,i);
  }

  Serial.println(sum);
  
}

