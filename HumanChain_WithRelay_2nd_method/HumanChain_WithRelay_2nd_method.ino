int ledPin = 13;      // select the pin for the LED
int firstPin = 2;      // select the pin for the LE
int relPins[5] = {2,3,4,5,6};      // select the pin for the 
int analogPins[5] = {A0,A1,A2,A3,A4};      // select the pin for the LED
int analogReads[5] = {0,0,0,0,0};      // select the pin for the LED

unsigned long timer = 0;
unsigned long timer_threshold = 50;

int relay_progression_counter = 0;
int relay_progression_counter_next = 1;

short touch_flags[10] = {0,0,0,0,0,0,0,0,0,0};


//#define DEBUG

void setup() {
  // declare the ledPin as an OUTPUT:
  pinMode(ledPin, OUTPUT);
  for(int k = 0; k<5; k++)
  {
    pinMode(relPins[k], OUTPUT);
    digitalWrite(relPins[k], HIGH);
  }
  digitalWrite(firstPin, LOW);
  Serial.begin(115200);
}

void loop() {
  
  if(millis() - timer > timer_threshold)
  {
    digitalWrite(relPins[relay_progression_counter], !digitalRead(relPins[relay_progression_counter]) );
    delayMicroseconds(100);
    if((relay_progression_counter - 1) < 0)
    {
      digitalWrite(relPins[4], !digitalRead(relPins[4]) ); 
    } else
      {
        digitalWrite(relPins[relay_progression_counter - 1], !digitalRead(relPins[relay_progression_counter - 1]) ); 
      }
    relay_progression_counter++;
    //relay_progression_counter_next++;

    if(relay_progression_counter == 5)
    {
        relay_progression_counter = 0;
    }
    
   /* if(relay_progression_counter_next == 5)
    {
        relay_progression_counter_next = 0;
    }*/
    
    timer = millis();   
  }

  if(relay_progression_counter == 0)
  {
    for(int k = 0; k<5; k++)
    {
      analogReads[k] = analogRead(analogPins[k]);
      if((analogReads[k] < 1010) && (k == 0))
      {
        touch_flags[0] = 1;
      } else
        {       
         touch_flags[0] = 0;
        }
        
      if((analogReads[k] < 1010) && (k == 1))
      {
        touch_flags[0] = 1;
      } else
        {       
         touch_flags[0] = 0;
        }
        
      if((analogReads[k] < 1010) && (k == 2))
      {
        touch_flags[1] = 1;
      } else
        {       
         touch_flags[1] = 0;
        }
        
      if((analogReads[k] < 1010) && (k == 3))
      {
        touch_flags[2] = 1;
      } else
        {       
         touch_flags[2] = 0;
        }

      if((analogReads[k] < 1010) && (k == 4))
      {
        touch_flags[3] = 1;
      } else
        {       
         touch_flags[3] = 0;
        }
        
      #ifdef DEBUG
        Serial.print(analogReads[k]); 
        Serial.print(" ");   
      #endif
    }
    #ifdef DEBUG
        Serial.println();  
    #endif
  }

  if(relay_progression_counter == 1)
  {
    for(int k = 0; k<5; k++)
    {
      analogReads[k] = analogRead(analogPins[k]);
      if((analogReads[k] < 1010) && (k == 0))
      {
        touch_flags[0] = 1;
      } else
        {       
         touch_flags[0] = 0;
        }
        
      if((analogReads[k] < 1010) && (k == 1))
      {
        touch_flags[0] = 1;
      } else
        {       
         touch_flags[0] = 0;
        }
        
      if((analogReads[k] < 1010) && (k == 2))
      {
        touch_flags[4] = 1;
      } else
        {       
         touch_flags[4] = 0;
        }
        
      if((analogReads[k] < 1010) && (k == 3))
      {
        touch_flags[5] = 1;
      } else
        {       
         touch_flags[5] = 0;
        }

      if((analogReads[k] < 1010) && (k == 4))
      {
        touch_flags[6] = 1;
      } else
        {       
         touch_flags[6] = 0;
        }
        
      #ifdef DEBUG
        Serial.print(analogReads[k]); 
        Serial.print(" ");   
      #endif
    }
    #ifdef DEBUG
        Serial.println();  
    #endif
  }

  if(relay_progression_counter == 2)
  {
    for(int k = 0; k<5; k++)
    {
      analogReads[k] = analogRead(analogPins[k]);
      if((analogReads[k] < 1010) && (k == 0))
      {
        touch_flags[1] = 1;
      } else
        {       
         touch_flags[1] = 0;
        }
        
      if((analogReads[k] < 1010) && (k == 1))
      {
        touch_flags[4] = 1;
      } else
        {       
         touch_flags[4] = 0;
        }
        
      if((analogReads[k] < 1010) && (k == 2))
      {
        touch_flags[4] = 1;
      } else
        {       
         touch_flags[4] = 0;
        }
        
      if((analogReads[k] < 1010) && (k == 3))
      {
        touch_flags[7] = 1;
      } else
        {       
         touch_flags[7] = 0;
        }

      if((analogReads[k] < 1010) && (k == 4))
      {
        touch_flags[8] = 1;
      } else
        {       
         touch_flags[8] = 0;
        }
        
      #ifdef DEBUG
        Serial.print(analogReads[k]); 
        Serial.print(" ");   
      #endif
    }
    #ifdef DEBUG
        Serial.println();  
    #endif
  }

  if(relay_progression_counter == 3)
  {
    for(int k = 0; k<5; k++)
    {
      analogReads[k] = analogRead(analogPins[k]);
      if((analogReads[k] < 1010) && (k == 0))
      {
        touch_flags[2] = 1;
      } else
        {       
         touch_flags[2] = 0;
        }
        
      if((analogReads[k] < 1010) && (k == 1))
      {
        touch_flags[5] = 1;
      } else
        {       
         touch_flags[5] = 0;
        }
        
      if((analogReads[k] < 1010) && (k == 2))
      {
        touch_flags[7] = 1;
      } else
        {       
         touch_flags[7] = 0;
        }
        
      if((analogReads[k] < 1010) && (k == 3))
      {
        touch_flags[7] = 1;
      } else
        {       
         touch_flags[7] = 0;
        }

      if((analogReads[k] < 1010) && (k == 4))
      {
        touch_flags[9] = 1;
      } else
        {       
         touch_flags[9] = 0;
        }
        
      #ifdef DEBUG
        Serial.print(analogReads[k]); 
        Serial.print(" ");   
      #endif
    }
    #ifdef DEBUG
        Serial.println();  
    #endif
  }

  if(relay_progression_counter == 4)
  {
    for(int k = 0; k<5; k++)
    {
      analogReads[k] = analogRead(analogPins[k]);
      if((analogReads[k] < 1010) && (k == 0))
      {
        touch_flags[3] = 1;
      } else
        {       
         touch_flags[3] = 0;
        }
        
      if((analogReads[k] < 1010) && (k == 1))
      {
        touch_flags[6] = 1;
      } else
        {       
         touch_flags[6] = 0;
        }
        
      if((analogReads[k] < 1010) && (k == 2))
      {
        touch_flags[8] = 1;
      } else
        {       
         touch_flags[8] = 0;
        }
        
      if((analogReads[k] < 1010) && (k == 3))
      {
        touch_flags[9] = 1;
      } else
        {       
         touch_flags[9] = 0;
        }

      if((analogReads[k] < 1010) && (k == 4))
      {
        touch_flags[9] = 1;
      } else
        {       
         touch_flags[9] = 0;
        }
        
      #ifdef DEBUG
        Serial.print(analogReads[k]); 
        Serial.print(" ");   
      #endif
    }
    #ifdef DEBUG
        Serial.println();  
    #endif
  }
  
  int sum = 0;
  for(int i = 0; i<10; i++)
  {
    sum += touch_flags[i]*pow(2,i);
  }

  Serial.println(sum);


  // stop the program
  delay(20);
}

