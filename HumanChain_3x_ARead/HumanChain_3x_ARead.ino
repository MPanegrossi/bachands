/*
  Analog Input
 Demonstrates analog input by reading an analog sensor on analog pin 0 and
 turning on and off a light emitting diode(LED)  connected to digital pin 13. 
 The amount of time the LED will be on and off depends on
 the value obtained by analogRead(). 
 
 The circuit:
 * Potentiometer attached to analog input 0
 * center pin of the potentiometer to the analog pin
 * one side pin (either one) to ground
 * the other side pin to +5V
 * LED anode (long leg) attached to digital output 13
 * LED cathode (short leg) attached to ground
 
 * Note: because most Arduinos have a built-in LED attached 
 to pin 13 on the board, the LED is optional.
 
 
 Created by David Cuartielles
 modified 30 Aug 2011
 By Tom Igoe
 
 This example code is in the public domain.
 
 http://arduino.cc/en/Tutorial/AnalogInput
 
 */

int sensorPin1 = A0;    // select the input pin for the potentiometer
int sensorPin2 = A1;    // select the input pin for the potentiometer
int sensorPin3 = A2;    // select the input pin for the potentiometer


int ledPin = 13;      // select the pin for the LED
  // Pin 13: Arduino has an LED connected on pin 13
  // Pin 11: Teensy 2.0 has the LED on pin 11
  // Pin  6: Teensy++ 2.0 has the LED on pin 6
  // Pin 13: Teensy 3.0 has the LED on pin 13
int sensorValue1 = 0;  // variable to store the value coming from the sensor
int sensorValue2 = 0;  // variable to store the value coming from the sensor
int sensorValue3 = 0;  // variable to store the value coming from the sensor



void setup() {
  // declare the ledPin as an OUTPUT:
  pinMode(ledPin, OUTPUT);
  Serial.begin(115200);
}

void loop() {
  // read the value from the sensor:
  sensorValue1 = analogRead(sensorPin1);  
  Serial.print("1 - ");
  Serial.print(sensorValue1);
  Serial.print(" ");
  if(sensorValue1 < 1010){
  digitalWrite(ledPin, HIGH);  
  } else{       
    digitalWrite(ledPin, LOW);  
  }
  sensorValue2 = analogRead(sensorPin2);  
  Serial.print("2 - ");  
  Serial.print(sensorValue2); 
  Serial.print(" ");
  if(sensorValue2 < 1010){
  digitalWrite(ledPin, HIGH);  
  } else{       
    digitalWrite(ledPin, LOW);  
  }
  sensorValue3 = analogRead(sensorPin3);  
  Serial.print("3 - ");  
  Serial.println(sensorValue3); 
  if(sensorValue3 < 1010){
  digitalWrite(ledPin, HIGH);  
  } else{       
    digitalWrite(ledPin, LOW);  
  }
  // stop the program for for <sensorValue> milliseconds:
  delay(20);                  
}

