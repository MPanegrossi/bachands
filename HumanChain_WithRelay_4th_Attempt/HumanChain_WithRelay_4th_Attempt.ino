int ledPin = 13;      // select the pin for the LED
int firstPin = 2;      // select the pin for the LE
int relPins[5] = {2,3,4,5,6};      // select the pin for the 
int analogPins[5] = {A0,A1,A2,A3,A4};      // select the pin for the LED
int analogReads[5] = {0,0,0,0,0};      // select the pin for the LED

unsigned long timer = 0;
unsigned long timer_threshold = 1000;

int relay_progression_counter = 0;
int relay_progression_counter_next = 1;

short touch_flags[10] = {0,0,0,0,0,0,0,0,0,0};

int delay_time_first_flip = 200;

bool yes = false;

#define DEBUG

void setup() {
  // declare the ledPin as an OUTPUT:
  pinMode(ledPin, OUTPUT);
  for(int k = 0; k<5; k++)
  {
    pinMode(relPins[k], OUTPUT);
    digitalWrite(relPins[k], HIGH);
  }
  digitalWrite(firstPin, LOW);
  Serial.begin(115200);
}

void loop() {
  
  digitalWriteFast(relPins[0], LOW);
  digitalWriteFast(relPins[1], HIGH);
  digitalWriteFast(relPins[2], HIGH);
  digitalWriteFast(relPins[3], HIGH);
  digitalWriteFast(relPins[4], HIGH);
  delay(delay_time_first_flip);

    analogReads[0] = analogRead(analogPins[0]);
    if(analogReads[0] < 1010)
    {
      yes = true;
    } 

  digitalWriteFast(relPins[0], HIGH);
  digitalWriteFast(relPins[1], LOW);
  digitalWriteFast(relPins[2], LOW);
  digitalWriteFast(relPins[3], LOW);
  digitalWriteFast(relPins[4], LOW);
  delay(delay_time_first_flip);

  readAN();


  digitalWriteFast(relPins[0], HIGH);
  digitalWriteFast(relPins[1], LOW);
  digitalWriteFast(relPins[2], HIGH);
  digitalWriteFast(relPins[3], HIGH);
  digitalWriteFast(relPins[4], HIGH);
  delay(delay_time_first_flip);


  digitalWriteFast(relPins[0], LOW);
  digitalWriteFast(relPins[1], HIGH);
  digitalWriteFast(relPins[2], LOW);
  digitalWriteFast(relPins[3], LOW);
  digitalWriteFast(relPins[4], LOW);
  delay(delay_time_first_flip);



  digitalWriteFast(relPins[0], HIGH);
  digitalWriteFast(relPins[1], HIGH);
  digitalWriteFast(relPins[2], LOW);
  digitalWriteFast(relPins[3], HIGH);
  digitalWriteFast(relPins[4], HIGH);
  delay(delay_time_first_flip);


  digitalWriteFast(relPins[0], LOW);
  digitalWriteFast(relPins[1], LOW);
  digitalWriteFast(relPins[2], HIGH);
  digitalWriteFast(relPins[3], LOW);
  digitalWriteFast(relPins[4], LOW);
  delay(delay_time_first_flip);


  digitalWriteFast(relPins[0], HIGH);
  digitalWriteFast(relPins[1], HIGH);
  digitalWriteFast(relPins[2], HIGH);
  digitalWriteFast(relPins[3], LOW);
  digitalWriteFast(relPins[4], HIGH);
  delay(delay_time_first_flip);


  digitalWriteFast(relPins[0], LOW);
  digitalWriteFast(relPins[1], LOW);
  digitalWriteFast(relPins[2], LOW);
  digitalWriteFast(relPins[3], HIGH);
  digitalWriteFast(relPins[4], LOW);
  delay(delay_time_first_flip);


  digitalWriteFast(relPins[0], HIGH);
  digitalWriteFast(relPins[1], HIGH);
  digitalWriteFast(relPins[2], HIGH);
  digitalWriteFast(relPins[3], HIGH);
  digitalWriteFast(relPins[4], LOW);
  delay(delay_time_first_flip);


  digitalWriteFast(relPins[0], LOW);
  digitalWriteFast(relPins[1], LOW);
  digitalWriteFast(relPins[2], LOW);
  digitalWriteFast(relPins[3], LOW);
  digitalWriteFast(relPins[4], HIGH);
  delay(delay_time_first_flip);

  
  
  
  int sum = 0;
  for(int i = 0; i<10; i++)
  {
    sum += touch_flags[i]*pow(2,i);
  }

  Serial.println(sum);
   
  // stop the program
  delay(20);
}

void readAN1()
{
    //Read All Other Pins
    analogReads[1] = analogRead(relPins[1]);
    analogReads[2] = analogRead(relPins[2]);
    analogReads[3] = analogRead(relPins[3]);
    analogReads[4] = analogRead(relPins[4]);

    if((analogReads[0] < 1010) && (analogReads[1] < 1010))
    {
      touch_flags[0] = 1;
    } else
      {
        touch_flags[0] = 0;
      }
    if((analogReads[0] < 1010) && (analogReads[2] < 1010))
    {
      touch_flags[1] = 1;
    } else
      {
        touch_flags[1] = 0;
      }
    if((analogReads[0] < 1010) && (analogReads[3] < 1010))
    {
      touch_flags[2] = 1;
    } else
      {
        touch_flags[2] = 0;
      }
    if((analogReads[0] < 1010) && (analogReads[4] < 1010))
    {
      touch_flags[3] = 1;
    } else
      {
        touch_flags[3] = 0;
      }

    #ifdef DEBUG
      for(int k = 0; k<5; k++)
      {
        Serial.print(analogReads[k]); 
        Serial.print(" ");  
      } 
      Serial.println();
    #endif
  
}

void readAN()
{
 //Read the five values



    #ifdef DEBUG
      Serial.print(analogReads[0]); 
      Serial.print(" ");   
    #endif

    analogReads[1] = analogRead(analogPins[1]);
    if(analogReads[1] < 1010  && yes)
    {
      touch_flags[1] = 1;
    } else
      {       
       touch_flags[1] = 0;
      } 

    #ifdef DEBUG
      Serial.print(analogReads[1]); 
      Serial.print(" ");   
    #endif

    analogReads[2] = analogRead(analogPins[2]);
    if(analogReads[2] < 1010  && yes)
    {
      touch_flags[2] = 1;
    } else
      {       
       touch_flags[2] = 0;
      } 

    #ifdef DEBUG
      Serial.print(analogReads[0]); 
      Serial.print(" ");   
    #endif

    analogReads[3] = analogRead(analogPins[3]);
    if(analogReads[3] < 1010  && yes)
    {
      touch_flags[3] = 1;
    } else
      {       
       touch_flags[3] = 0;
      } 

    #ifdef DEBUG
      Serial.print(analogReads[0]); 
      Serial.print(" ");   
    #endif

        analogReads[4] = analogRead(analogPins[4]);
    if(analogReads[4] < 1010  && yes)
    {
      touch_flags[4] = 1;
    } else
      {       
       touch_flags[4] = 0;
      } 

    #ifdef DEBUG
      Serial.print(analogReads[0]); 
      Serial.print(" ");   
    #endif
  
  #ifdef DEBUG
    Serial.println();
  #endif
}


