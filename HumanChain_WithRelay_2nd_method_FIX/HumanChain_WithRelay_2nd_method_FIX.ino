int ledPin = 13;      // select the pin for the LED
int firstPin = 2;      // select the pin for the LE
int relPins[5] = {2,3,4,5,6};      // select the pin for the 
int analogPins[5] = {A0,A1,A2,A3,A4};      // select the pin for the LED
int analogReads[5] = {0,0,0,0,0};      // select the pin for the LED

unsigned long timer = 0;
unsigned long timer_threshold = 50;

int relay_progression_counter = 0;
int relay_progression_counter_next = 1;

short touch_flags[10] = {0,0,0,0,0,0,0,0,0,0};


//#define DEBUG

void setup() {
  // declare the ledPin as an OUTPUT:
  pinMode(ledPin, OUTPUT);
  for(int k = 0; k<5; k++)
  {
    pinMode(relPins[k], OUTPUT);
    digitalWrite(relPins[k], LOW);
  }
  digitalWrite(firstPin, LOW);
  Serial.begin(115200);
}

void loop() {
  
  if(millis() - timer > timer_threshold)
  {
    digitalWrite(relPins[relay_progression_counter], !digitalRead(relPins[relay_progression_counter]) );
    delayMicroseconds(100);
    if(relay_progression_counter == 0)
    {
      if(digitalRead(relPins[4]) == HIGH)
      {
        digitalWrite(relPins[4], !digitalRead(relPins[4]) );
      }
    } else
      {
        digitalWrite(relPins[relay_progression_counter - 1], !digitalRead(relPins[relay_progression_counter - 1]) ); 
      }
    relay_progression_counter++;
    //relay_progression_counter_next++;

    if(relay_progression_counter == 5)
    {
        relay_progression_counter = 0;
    }
    
   /* if(relay_progression_counter_next == 5)
    {
        relay_progression_counter_next = 0;
    }*/
    
    timer = millis();   
  }

  if((relay_progression_counter - 1) == -1)
  {

      analogReads[0] = analogRead(analogPins[0]);
      if((analogReads[0] < 800))
      {
        touch_flags[0] = 1;
      } else
        {       
         touch_flags[0] = 0;
        }
      
      analogReads[1] = analogRead(analogPins[1]);
      if((analogReads[1] < 800))
      {
        touch_flags[0] = 1;
      } else
        {       
         touch_flags[0] = 0;
        }
        
      analogReads[2] = analogRead(analogPins[2]);
      if((analogReads[2] < 800))
      {
        touch_flags[1] = 1;
      } else
        {       
         touch_flags[1] = 0;
        }
      
      analogReads[3] = analogRead(analogPins[3]);
      if((analogReads[3] < 800))
      {
        touch_flags[2] = 1;
      } else
        {       
         touch_flags[2] = 0;
        }
        
      analogReads[4] = analogRead(analogPins[4]);
      if((analogReads[4] < 800))
      {
        touch_flags[3] = 1;
      } else
        {       
         touch_flags[3] = 0;
        }
        
      #ifdef DEBUG
        for(int k = 0; k<5; k++)
        {
          Serial.print(analogReads[k]); 
          Serial.print(" ");  
        }
        Serial.println(); 
      #endif
  }

  if((relay_progression_counter - 1) == 0)
  {
      analogReads[0] = analogRead(analogPins[0]);
      if((analogReads[0] < 800))
      {
        touch_flags[0] = 1;
      } else
        {       
         touch_flags[0] = 0;
        }
      
      analogReads[1] = analogRead(analogPins[1]);
      if((analogReads[1] < 800))
      {
        touch_flags[0] = 1;
      } else
        {       
         touch_flags[0] = 0;
        }
        
      analogReads[2] = analogRead(analogPins[2]);
      if((analogReads[2] < 800))
      {
        touch_flags[4] = 1;
      } else
        {       
         touch_flags[4] = 0;
        }
      
      analogReads[3] = analogRead(analogPins[3]);
      if((analogReads[3] < 800))
      {
        touch_flags[5] = 1;
      } else
        {       
         touch_flags[5] = 0;
        }
        
      analogReads[4] = analogRead(analogPins[4]);
      if((analogReads[4] < 800))
      {
        touch_flags[6] = 1;
      } else
        {       
         touch_flags[6] = 0;
        }
        
      #ifdef DEBUG
        for(int k = 0; k<5; k++)
        {
          Serial.print(analogReads[k]); 
          Serial.print(" ");  
        }
        Serial.println(); 
      #endif
  }

  if((relay_progression_counter - 1) == 1)
  {
      analogReads[0] = analogRead(analogPins[0]);
      if((analogReads[0] < 800))
      {
        touch_flags[1] = 1;
      } else
        {       
         touch_flags[1] = 0;
        }
      
      analogReads[1] = analogRead(analogPins[1]);
      if((analogReads[1] < 800))
      {
        touch_flags[4] = 1;
      } else
        {       
         touch_flags[4] = 0;
        }
        
      analogReads[2] = analogRead(analogPins[2]);
      if((analogReads[2] < 800))
      {
        touch_flags[4] = 1;
      } else
        {       
         touch_flags[4] = 0;
        }
      
      analogReads[3] = analogRead(analogPins[3]);
      if((analogReads[3] < 800))
      {
        touch_flags[7] = 1;
      } else
        {       
         touch_flags[7] = 0;
        }
        
      analogReads[4] = analogRead(analogPins[4]);
      if((analogReads[4] < 800))
      {
        touch_flags[8] = 1;
      } else
        {       
         touch_flags[8] = 0;
        }
        
      #ifdef DEBUG
        for(int k = 0; k<5; k++)
        {
          Serial.print(analogReads[k]); 
          Serial.print(" ");  
        }
        Serial.println(); 
      #endif
  }

  if((relay_progression_counter - 1) == 2)
  {
      analogReads[0] = analogRead(analogPins[0]);
      if((analogReads[0] < 800))
      {
        touch_flags[2] = 1;
      } else
        {       
         touch_flags[2] = 0;
        }
      
      analogReads[1] = analogRead(analogPins[1]);
      if((analogReads[1] < 800))
      {
        touch_flags[5] = 1;
      } else
        {       
         touch_flags[5] = 0;
        }
        
      analogReads[2] = analogRead(analogPins[2]);
      if((analogReads[2] < 800))
      {
        touch_flags[7] = 1;
      } else
        {       
         touch_flags[7] = 0;
        }
      
      analogReads[3] = analogRead(analogPins[3]);
      if((analogReads[3] < 800))
      {
        touch_flags[7] = 1;
      } else
        {       
         touch_flags[7] = 0;
        }
        
      analogReads[4] = analogRead(analogPins[4]);
      if((analogReads[4] < 800))
      {
        touch_flags[9] = 1;
      } else
        {       
         touch_flags[9] = 0;
        }
        
      #ifdef DEBUG
        for(int k = 0; k<5; k++)
        {
          Serial.print(analogReads[k]); 
          Serial.print(" ");  
        }
        Serial.println(); 
      #endif
  }

  if((relay_progression_counter - 1) == 3)
  {
      analogReads[0] = analogRead(analogPins[0]);
      if((analogReads[0] < 800))
      {
        touch_flags[3] = 1;
      } else
        {       
         touch_flags[3] = 0;
        }
      
      analogReads[1] = analogRead(analogPins[1]);
      if((analogReads[1] < 800))
      {
        touch_flags[6] = 1;
      } else
        {       
         touch_flags[6] = 0;
        }
        
      analogReads[2] = analogRead(analogPins[2]);
      if((analogReads[2] < 800))
      {
        touch_flags[8] = 1;
      } else
        {       
         touch_flags[8] = 0;
        }
      
      analogReads[3] = analogRead(analogPins[3]);
      if((analogReads[3] < 800))
      {
        touch_flags[9] = 1;
      } else
        {       
         touch_flags[9] = 0;
        }
        
      analogReads[4] = analogRead(analogPins[4]);
      if((analogReads[4] < 800))
      {
        touch_flags[9] = 1;
      } else
        {       
         touch_flags[9] = 0;
        }
        
      #ifdef DEBUG
        for(int k = 0; k<5; k++)
        {
          Serial.print(analogReads[k]); 
          Serial.print(" ");  
        }
        Serial.println(); 
      #endif
  }
  
  int sum = 0;
  for(int i = 0; i<10; i++)
  {
    sum += touch_flags[i]*pow(2,i);
  }

  Serial.println(sum);

  // stop the program
  delay(20);
}

