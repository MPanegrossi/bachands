int ledPin = 13;      // select the pin for the LED
int firstPin = 2;      // select the pin for the LE
int relPins[5] = {2,3,4,5,6};      // select the pin for the 
int analogPins[5] = {A0,A1,A2,A3,A4};      // select the pin for the LED
int analogReads[5] = {0,0,0,0,0};      // select the pin for the LED

unsigned long timer = 0;
unsigned long timer_threshold = 50;

int relay_progression_counter = 0;
int relay_progression_counter_next = 1;

byte touch_flags;


//#define DEBUG

void setup() {
  // declare the ledPin as an OUTPUT:
  pinMode(ledPin, OUTPUT);
  for(int k = 0; k<5; k++)
  {
    pinMode(relPins[k], OUTPUT);
    digitalWrite(relPins[k], HIGH);
  }
  digitalWrite(firstPin, LOW);
  Serial.begin(115200);
}

void loop() {
  
  if(millis() - timer > timer_threshold)
  {
    digitalWrite(relPins[relay_progression_counter], !digitalRead(relPins[relay_progression_counter]) );
    delayMicroseconds(100);
    digitalWrite(relPins[relay_progression_counter_next], !digitalRead(relPins[relay_progression_counter_next]) );  
    relay_progression_counter++;
    relay_progression_counter_next++;

    if(relay_progression_counter == 5)
    {
        relay_progression_counter = 0;
    }
    
    if(relay_progression_counter_next == 5)
    {
        relay_progression_counter_next = 0;
    }
    
    timer = millis();   
  }
  
  //Read the five values
  for(int k = 0; k<5; k++)
  {
    analogReads[k] = analogRead(analogPins[k]);
    if(analogReads[k] < 1010)
    {
      bitSet(touch_flags, k);
    } else
      {       
       bitClear(touch_flags, k);
      } 

    #ifdef DEBUG
      Serial.print(analogReads[k]); 
      Serial.print(" ");   
    #endif
  }
  
  #ifdef DEBUG
    Serial.println();
  #endif
  
  Serial.println(touch_flags);
   
  // stop the program
  delay(20);
}

